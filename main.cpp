
#include <iostream>
#include <cstdlib>
#include "klasy.h"

using namespace std;

void menu (void);
void wyjscie (void);

int main ()
{
    cout << "/**********************************************************************************************/" << endl
         << "	Program testujacy klase Obraz, reprezentujaca czarno-bialy obraz (za pomoca macierzy)." << endl << endl
         << "/**********************************************************************************************/" << endl << endl;

    Obraz nowy1, nowy2, nowy3; //tworzymy 3 obiekty na ktorych bedziemy pracowac
    int a, b;
    char ch, c;
    menu ();

    while (cin >> ch) //zapetlamy menu, zeby program nie zamknal sie po pierwszym tescie
    {

        switch (ch)
        {
        case '1':
            cout << nowy1;  //wypisuje tablice, sprawdzajac <<
            break;
        case '2':
            nowy3= nowy1 + nowy2;  //dodaje dwa obrazy i zapisuje pod trzecim.
            cout <<"pierwsze" << endl << nowy1 << endl ;
            cout <<"drugi" << endl  << nowy2 << endl ;
            cout <<"trzeci" << endl  << nowy3 << endl ;
            nowy3.wyczysc();  //czyscimy obraz, zeby potem nie bylo przeklaman
            break;
        case '3':
            cout << "jedynka"<< endl<< nowy1 <<endl <<"trojka" <<endl << nowy3 <<endl;
            nowy3 += nowy1;
            cout <<"wynik"<< endl<<  nowy3<<endl;
            nowy3.wyczysc();//czyscimy obraz, zeby potem nie bylo przeklaman
            break;
        case '4':
            nowy3= nowy1 * nowy2;
            cout << "jedynka" <<endl << nowy1 <<endl<< " dwojka"<<endl << nowy2 <<endl <<"trojka" <<endl << nowy3;
            nowy3.wyczysc();//czyscimy obraz, zeby potem nie bylo przeklaman
            break;
        case '5':
            cout << "pierwszy"<<endl<< nowy3 <<endl<<"drugi"<<endl<< nowy2;
            nowy3 *= nowy2;
            cout <<endl<<"wynik" <<endl <<nowy3;
            nowy3.wyczysc();//czyscimy obraz, zeby potem nie bylo przeklaman
            break;
        case '6':
            nowy1.lustro(); //uzywamy metody lustro, zeby uzyskac lustrzane odbicie i drukujemy obraz
            cout << nowy1;
            break;
        case '7':
            cout<< "Podaj wysokosc pixela (0-9)" <<endl; //prosimy o wspolrzedne punktu, ktory chcemy zmienic
            cin>>a;
            cout<< "Podaj szerokosc pixela (0-9)" <<endl;
            cin>>b;
            cout<< nowy1.zwroc(a, b);  //zwracamy wartosc konkretnego pixela

            break;
        case '8':
            cout<< "Podaj wysokosc pixela (0-9)" <<endl; //prosimy o wspolrzedne punktu, ktory chcemy zmienic
            cin>>a;
            cout<< "Podaj szerokosc pixela (0-9)" <<endl;
            cin>>b;
            cout<< "Czy pixel ma byc pusty? y/n " <<endl;
            cin>>c; //pytanie ograniczajace wybor uzytkownika, jedynie czarny lub bialy
            if(c=='n') c='B';
            else c='C';
            nowy1.zmien(a, b, c);
            break;
        case '9':
            wyjscie();
            break;
        }
        menu (); //wyswietlamy ponownie menu
    }

    return 0;
}

void menu (void) //menu programu testujacego
{

    cout << endl;
    cout << "(1) Sprawdz dzialanie operatora <<" << endl;
    cout << "(2) Sprawdz dzialanie operatora +" << endl;
    cout << "(3) Sprawdz dzialanie operatora +=" << endl;
    cout << "(4) Sprawdz dzialanie operatora *" << endl;
    cout << "(5) Sprawdz dzialanie operatora *=" << endl;
    cout << "(6) Sprawdz dzialanie odbicia lustrzanego" << endl;
    cout << "(7) Sprawdz dzialanie odczytu wartosci" << endl;
    cout << "(8) Sprawdz dzialanie zmiana wartosci" << endl;
    cout << "(9) Zakoncz program" << endl;

}


void wyjscie (void)//funkcja zamykajaca program
{
    exit (EXIT_SUCCESS);
}

