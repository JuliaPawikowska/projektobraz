#include "klasy.h"
#include <iostream>
#include <stdlib.h>

Obraz::Obraz() //konstruktor klasy, wypelnia caly obraz "randomowymi" wartosciami, aby bylo latwiej testowac
{

    int i, j;
    for(i=0; i<10; i++)
    {
        for(j=0; j<10; j++)
        {
            if(rand()%2 == 0) //randomowanie wartosci
                tablica[i][j]='B';
            else
                tablica[i][j]='C';
        }
    }
    std::cout << "Tworzenie obrazu. " << std::endl;  //konstruktor oznajmia, ze zostal uzyty
}


void Obraz::wypelnijpunkt(int &a, int &b) //metoda koloruje pixel
{
    tablica[a][b]='B';
}
void Obraz::wyczysc() //metoda czyszczaca obiekt
{
    int i, j;
    for(i=0; i<10; i++)
    {
        for(j=0; j<10; j++)
        {
            tablica[i][j]='C';
        }
    }
}

void Obraz::zmien(int a, int b, char c) //metoda zmieniajaca wartosc koloru
{
    tablica[a][b] = c;

}

void Obraz::usunpunkt(const int &a, const int &b) //metoda usuwa pojedynczy pixel
{
    tablica[a][b]='C';
}



char Obraz::zwroc(int a, int b)  //metoda odczytująca wartość pixela
{
    return this->tablica[a][b];
}

void Obraz::lustro() //lustrzane odbicie
{
    int k, l;
    int tablicatmp[10][10];                 //dodatkowa tablica, do przechowywania danych
    for(k=0; k<10; k++)
        for(l=0; l<10; l++)
            tablicatmp[k][l]=tablica[k][l];    //"lustrzana" zamiana pixeli przy pomocy
    for(k=0; k<10; k++)
        for(l=0; l<10; l++)
            tablica[k][l]=tablicatmp[k][9-l];  //dotakowej tablicy
}


Obraz Obraz::operator+(Obraz &f)//przeciazanie + (w sume logiczna)
{
    int i, j;
    Obraz ob1;
    ob1.wyczysc();      //najpierw czyscimy obiekt
    for(i=0; i<10; i++)
    {
        for(j=0; j<10; j++)
        {
            if (this->zwroc(i, j)=='B'||f.zwroc(i, j)=='B') ob1.wypelnijpunkt(i, j);  //sumujemy "logicznie" po kazdej komorce
        }
    }
    return ob1;
}

void Obraz::operator+=(Obraz &f)//przeciazanie + (w sume logiczna)
{
    int i, j;
    for(i=0; i<10; i++)
    {
        for(j=0; j<10; j++)
        {
            if (zwroc(i, j)=='B'||f.zwroc(i, j)=='B')    wypelnijpunkt(i, j);
        }
    }
}


Obraz Obraz::operator*(Obraz &f) //przeciazanie * w iloczyn logiczny
{
    int i, j;
    Obraz ob1;
    for(i=0; i<10; i++)
    {
        for(j=0; j<10; j++)
        {
            if (zwroc(i, j)=='C'||f.zwroc(i, j)=='C')    ob1.usunpunkt(i, j);  //iloczyn logiczny po kazdej komorce
        }
    }
    return ob1;
}

void Obraz::operator*=(Obraz &f) //przeciazanie *= w iloczyn logiczny
{
    int i, j;
    for(i=0; i<10; i++)
    {
        for(j=0; j<10; j++)
        {
            if (zwroc(i, j)=='C'||f.zwroc(i, j)=='C')    usunpunkt(i, j);
        }
    }
}


std::ostream & operator<< (std::ostream & out, Obraz & ob1) //przeciazanie operatora <<
{
    int k, l;
    for(k=0; k<10; k++)
    {
        for(l=0; l<10; l++)
        {
            std::cout<< ob1.tablica[k][l] << " ";  //wypisuje od razu cala tablice z odstepami miedzy kolorami
        }
        std::cout<<std::endl;
    }
    return out;
}


