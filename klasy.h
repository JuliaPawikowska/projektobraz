#ifndef KLASY_H
#define KLASY_H

#include <iostream>
#include <cstring>
#include <cctype>


class Obraz // nazwa klasy
{
private:
    char tablica[10][10];  //w czesci prywatnej tylko tablica, do ktorej dostep uzyskamy poprzez metody

public:

    Obraz(); //konstruktor
    void usunpunkt(const int &a, const int &b);
    void wypelnijpunkt(int &a, int &b);
    char zwroc(int a, int b);
    void lustro();
    void wyczysc();
    void zmien(int a, int b, char c);
    Obraz operator+(Obraz &f);
    void operator+= (Obraz &f);
    Obraz operator*(Obraz &f);
    void operator*=(Obraz &f);
    ~Obraz() {}; //nie ma alokacji pami�ci, wi�c nie musimy nic zwalnia� (tablica statyczna)
    friend std::ostream & operator<< (std::ostream & out, Obraz & ob1);
};
std::ostream & operator<< (std::ostream & out, Obraz & ob1);
#endif
